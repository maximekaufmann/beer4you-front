import {Link} from "react-router-dom"
import Logo from "../assets/logo/logo-beer.jpg"

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCartShopping, faHome, faPersonFalling, faGears, faBeerMugEmpty , faRightFromBracket} from '@fortawesome/free-solid-svg-icons'


const Header = (props) => {
    //fonction de deconnexion d'un utilisateur
    const logoutUser = () => {
        //on détruit le token
        
        //on réinitialise le store de redux
    
    }
    return (<header className="header-nav">
        <nav>
            <div className="list1">
                <img src={Logo} alt="logo Beer4You"/>
                <Link to="/"><FontAwesomeIcon icon={faHome} /></Link>
                <Link to="/product"><FontAwesomeIcon icon={faBeerMugEmpty} /></Link>
            </div>
            
            {/* menu en mode connecté ou déconnecté */}
        </nav>
        <section className="header-pict">
            <div className="background_opacity"></div>
            <h1>Beer4you, de la mauvaise bière, mais de la bière quand même !</h1>
        </section>
    </header>)
}

export default Header