import Header from "./components/header"
import Home from "./containers/home"
import './App.css'

import {Routes, Route, Navigate} from "react-router-dom"
import RequireAuth from "./helpers/require-auth"

function App() {

  
  return (
    <div className="App">
      <Header/>
      <Routes>
        <Route path='/' element={<RequireAuth child={Home} auth={false} admin={false} />}/>
        
        <Route path='*' element={<Navigate to="/" />}/>
      </Routes>
    </div>
  )
}

export default App
