import {useState, useEffect} from "react"
import {useSelector, useDispatch} from "react-redux"
import {Navigate, useParams} from "react-router-dom"
//import des actions et les states globales



//ici on importe une fonction api pour récupérer les bières


//HOC de controle des datas et de la sécurité des routes
const RequireAuth = (props) => {
    //on récup les params de la route
    const params = useParams()
    //on récupère les states globales user et beer dans le store en mode lecture
    
    
    //on prépare la fonctionnalité pour dispatcher notre action dans le store de redux
    
    //on récupère le composant à afficher qui a été passé en tant que props
    const Child = props.child
    //gestion de la redirection
    const [redirect, setRedirect] = useState(false)
    const [redirectAdmin, setRedirectAdmin] = useState(false)
    
    
    //lorsque les props de notre composant sont chargés
    useEffect(()=>{
        //si les bières ne sont pas chargée dans redux on les charges
        


        //on va tester si on est connecté via les infos de redux
        //si l'utilisateur n'est pas connecté
        
            //on récup le token dans le storage
            
            //si le storage répond null (pas trouve) et que la props auth est true (route protégée)
            
                //accés vers la route refusée
                
            //sinon
            
                //si le token n'est pas null
                
                    //on appel notre requète ajax qui va vérifier le token dans le back
                    
                    //then
                        
                        //si la réponse n'est pas 200
                        
                            //si la route est protégée
                            
                                //accés vers la route refusée
                                
                        //sinon
                        
                            //on stock la réponse de la requète ajax
                            
                            //on peut rajouter le token à l'objet aussi si on veut
                            
                            //appel de l'action de connexion de l'utilisateur dans le store de redux
                            
                            //on vérifie si jamais la route demandé est admin que son role soit admin
                            
                                //accés à la route refusée
                                
        //sinon
        
            //un utilisateur est connecté dans le store redux
            //si le role n'est pas admin et que la props admin est true (route protégée d'admin)
                
                //accés à la route refusée
                
    }, [props])
    
    if(redirect){
        return <Navigate to="/login" />
    }
    if(redirectAdmin){
        return <Navigate to="/" />
    }
    return (<Child {...props} params={params} />)
}


export default RequireAuth

